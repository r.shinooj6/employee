import { Component, ViewChild } from '@angular/core';


import { MatTable } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { DialogBoxComponent } from './dialog-box/dialog-box.component';
import { DataAccessService } from './data-access.service';
import { JsonPipe } from '@angular/common';


export interface UsersData {
  name: string;
  id: number;
}

const ELEMENT_DATA: UsersData[] = [
  {id: 1560608769632, name: 'Artificial Intelligence'},
  {id: 1560608796014, name: 'Machine Learning'},
  {id: 1560608787815, name: 'Robotic Process Automation'},
  {id: 1560608805101, name: 'Blockchain'}
];
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'EmployeeApp';



  displayedColumns: string[] = ['id', 'name', 'age' ,'address', 'action'];
 // dataSource = ELEMENT_DATA;
 dataSource:any;

  @ViewChild(MatTable,{static:true}) table: MatTable<any>;

  constructor(public dialog: MatDialog,private dataAccessService:DataAccessService) {}

  ngOnInit(): void {
  
  this.dataAccessService.getEmp()
  .subscribe(
  data => {
    console.log(data);
    this.dataSource=data;
  },
  error => {
  
  console.log(error.error.message);
  
  });
  }
  openDialog(action,obj) {
    obj.action = action;
    const dialogRef = this.dialog.open(DialogBoxComponent, {
      width: '250px',
      data:obj
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result.event == 'Add'){
        this.addRowData(result.data);
      }else if(result.event == 'Update'){
        this.updateRowData(result.data);
      }else if(result.event == 'Delete'){
        this.deleteRowData(result.data);
      }
    });
  }

  addRowData(row_obj){
    delete row_obj.action;
    
   console.log("add:----"+JSON.stringify(row_obj));

   this.dataAccessService.addEmp(row_obj).subscribe(
  data => {
    console.log("added Emp:::\n"+data);
    location.reload();
  },
  error => {
  
  console.log(error.error.message);
  
  });
    //this.table.renderRows();
    
  }


  updateRowData(row_obj){
    delete row_obj.action;
    console.log("updated ::"+JSON.stringify(row_obj))
    this.dataAccessService.updateEmp(row_obj)
  .subscribe(
  data => {
    console.log("updated response=="+data);
    location.reload();
  },
  error => {
  
  console.log(error.error.message);
  
  });
  }


  deleteRowData(row_obj){
    this.dataAccessService.remEmp(row_obj.id)
  .subscribe(
  data => {
    console.log("Deleted response=="+data);
    location.reload();
  },
  error => {
  
  console.log(error.error.message);
  
  });
   
  }

}
