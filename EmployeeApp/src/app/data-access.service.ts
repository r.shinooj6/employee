import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Employee } from './Employee';
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  

@Injectable({
  providedIn: 'root'
})
export class DataAccessService {


  headers = new HttpHeaders().set('Content-Type', 'application/json').set('Accept', 'application/json');
  httpOptions = {
    headers: this.headers
  };
  baseUrl:String='http://localhost:8088/api'
  constructor(private http: HttpClient) { }

  getEmp() {
    
    return this.http.get(this.baseUrl+`/emp`);
    }

  addEmp(emp:Employee):Observable<Employee>{
   
    emp.id=null;
    return this.http.post<Employee>(this.baseUrl+`/emp/insert`,emp,this.httpOptions);
  }

  updateEmp(upEmp:Employee):Observable<Employee>{
   
    return this.http.put<Employee>(this.baseUrl+`/emp/update`,upEmp,this.httpOptions);
  }

  remEmp(empId){
    return this.http.delete(this.baseUrl+`/emp/delete/`+empId,{ ...Option, responseType: 'text' });
  }
}
