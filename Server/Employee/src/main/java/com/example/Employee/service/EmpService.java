package com.example.Employee.service;

import com.example.Employee.exception.EmployeeAlreadyExistsException;
import com.example.Employee.exception.EmployeeNotFoundException;
import com.example.Employee.model.Employee;
import com.example.Employee.repository.EmpRepository;


import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmpService {
	
	@Autowired
	private EmpRepository empRepository;
	
	@Autowired
	public EmpService(EmpRepository empRepository)
	{
		this.empRepository=empRepository;
	}
	
	
	//getAll
	
	
	public Iterable<Employee> getAllEmployees() {
		return empRepository.findAll();
	}
	

	
	//get employee by ID
	public Employee getEmployeeById(int id) throws EmployeeNotFoundException {

		Optional<Employee> empExists = empRepository.findById(id);
		if (empExists.isPresent()) {
			return empExists.get();
		} else {
			throw new EmployeeNotFoundException();
		}
	}
	
	
	//insert employee
	public Employee saveEmployee(Employee emp) throws EmployeeAlreadyExistsException {
		Optional<Employee> empExists = empRepository.findById(emp.getId());
		if (empExists.isPresent()) {
			throw new EmployeeAlreadyExistsException();
		} else {
			return empRepository.save(emp);
		}
	}
	
	//update employee
	public Employee updateEmployee(Employee emp) throws EmployeeNotFoundException {
		Optional<Employee> empExists = empRepository.findById(emp.getId());
		if (empExists.isPresent()) {
			return empRepository.save(emp);

		} else {
			throw new EmployeeNotFoundException();
		}
	}
	
	
	//Delete Employee
	public Object deleteEmployee(int id) throws EmployeeNotFoundException {
		Optional<Employee> empExists = empRepository.findById(id);
		if (empExists.isPresent()) {
			empRepository.delete(empExists.get());
			return null;
		} else {
			throw new EmployeeNotFoundException();
		}
	}

	

	

}
