package com.example.Employee.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason ="Employee Not Found Exception..")
public class EmployeeNotFoundException extends Exception {

}
