package com.example.Employee.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.Employee.exception.EmployeeAlreadyExistsException;
import com.example.Employee.exception.EmployeeNotFoundException;
import com.example.Employee.model.Employee;
import com.example.Employee.service.EmpService;


@CrossOrigin(origins = "*", exposedHeaders = "Access-Control-Allow_Origin")
@RestController
@RequestMapping("api")
public class EmpController {
	
	@Autowired
	private EmpService empService;

	private ResponseEntity<?> responseEntity;

	@Autowired
	public EmpController(EmpService empService) {
		this.empService = empService;
	}
	
	
	// get all employee
	
	@GetMapping("/emp")
	public ResponseEntity<?> getAllEmployees() {
		try {
			Iterable<Employee> allEmployees = empService.getAllEmployees();
			responseEntity = new ResponseEntity<>(allEmployees, HttpStatus.OK);
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	
	//get Employee by id
	
	@GetMapping("/emp/{id}")
	public ResponseEntity<?> getEmployeeById(@PathVariable("id") int id) throws EmployeeNotFoundException {
		try {
			Employee emp = empService.getEmployeeById(id);
			responseEntity = new ResponseEntity<>(emp, HttpStatus.OK);

		} catch (EmployeeNotFoundException e) {
			throw e;
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	
	//Insert Employee
	
	@PostMapping("/emp/insert")
	public ResponseEntity<?> saveEmployee(@RequestBody Employee emp) throws EmployeeAlreadyExistsException {
		try {
			Employee createdEmployee = empService.saveEmployee(emp);
			responseEntity = new ResponseEntity<>(createdEmployee, HttpStatus.CREATED);
		} catch (EmployeeAlreadyExistsException e) {
			throw e;
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}

	
	//Update Employee

	@PutMapping("/emp/update")
	public ResponseEntity<?> updateEmployee(@RequestBody Employee emp) throws EmployeeNotFoundException {
		try {
			Employee updatedEmployee = empService.updateEmployee(emp);

			responseEntity = new ResponseEntity<>(updatedEmployee, HttpStatus.OK);

		} catch (EmployeeNotFoundException e) {
			throw e;
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
	
	//Delete Employee
	
	@DeleteMapping("/emp/delete/{id}")
	public ResponseEntity<?> deleteEmployee(@PathVariable("id") int id) throws EmployeeNotFoundException {
		try {
			 empService.deleteEmployee(id);
			responseEntity = new ResponseEntity<>("successfully Deleted", HttpStatus.OK);

		} catch (EmployeeNotFoundException e) {
			throw e;
		} catch (Exception e) {
			responseEntity = new ResponseEntity<>("Some internal error occured..", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return responseEntity;
	}
}
