package com.example.Employee.repository;

import java.util.List;

import com.example.Employee.model.Employee;

import org.springframework.stereotype.Repository;

import org.springframework.data.jpa.repository.JpaRepository;  

@Repository
public interface EmpRepository extends JpaRepository<Employee, Integer>{

}
